const $players = document.getElementById('players')
const $word = document.getElementById('word')
const $phrase = document.getElementById('phrase')
const $phrases = document.getElementById('phrases')
const $error = document.getElementById('error')
const $restart = document.getElementById('restart')

$word.textContent = '_'

const state = {
  leftside: false
}

const deepEqual = (a, b) => typeof a !== 'object'
  ? a === b
  : Object.entries(a)
    .every(([key, value]) => deepEqual(b[key], value))

const add = (leftside, word, char) => leftside ? char + word : word + char
const playerStyle = (turn, i) => turn % state.players.length === i
  ? 'player-active'
  : 'player-not-so-active'

const players = turn => state.players
  .map((player, i) => {
    const div = document.createElement('div')
    div.className = playerStyle(turn, i)
    div.textContent = player
    return div
  })
  .reduce((div, child) => {
    div.appendChild(child)
    return div
  }, document.createElement('div'))

const render = () => {
  $phrases.textContent = state.players.join(', ')
  $word.textContent = add(state.leftside, state.word, '_')
  if ($players.firstChild) {
    $players.firstChild.remove()
  }
  $players.appendChild(players(state.word.length))
}

const handleError = err => {
  $error.textContent = err.message
}

const update = data => {
  if (data.success) {
    $error.textContent = ''
    if (Object.entries(data.state)
      .every(([key, value]) => deepEqual(state[key], value))) {
      return
    }
    Object.entries(data.state)
      .forEach(([key, value]) => { state[key] = value })
    render()
  } else {
    $error.textContent = data.error
  }
}

const handleResponse = res => res.json()
  .then(json => {
    if (res.ok) {
      return json
    } else {
      throw new Error(json.error)
    }
  })

const fetch = (...args) => {
  const options = args.length
    ? {
      method: args[0],
      body: JSON.stringify(args[1]),
      headers: {
        'content-type': 'application/json;charset=utf-8'
      }
    }
    : {}

  window.fetch('/balda/api', {
    ...options
  })
    .then(handleResponse)
    .then(update)
    .catch(handleError)
}

const say = (char, leftside) => fetch('POST', {
  act: 'say',
  char,
  leftside
})

document.body.addEventListener('keypress', ev => {
  if (ev.charCode === 61 || ev.charCode === 43) {
    state.leftside = false
    render()
  } else if (ev.charCode === 45 || ev.charCode === 95) {
    state.leftside = true
    render()
  } else if (/[a-zа-яё]/.test(ev.key)) {
    say(ev.key, state.leftside)
  }
})

const giveName = name => fetch('PUT', { name })

$phrase.addEventListener('keypress', ev => {
  ev.stopPropagation()
  if (ev.keyCode === 13) {
    ev.preventDefault()
    giveName(ev.target.value)
  }
})

const restart = () => fetch('POST', { act: 'restart' })

$restart.addEventListener('click', ev => {
  ev.stopPropagation()
  restart()
})

fetch()
setInterval(fetch, 2000)
