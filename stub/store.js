const level = require('level')
const db = level('db')
// db.clear()
const knownPlayers = {}

const state = {
  players: [],
  word: ''
}

const forget = () => {
  const now = hit()

  state.players = state.players
    .filter(player => now - knownPlayers[player].lastHit < 120000)
}
setInterval(forget, 30000)

const hit = () => Number(Date.now())

const putPlayer = (UUID, name, cb) => {
  if (!name) {
    return cb(new Error('name must be set'))
  }
  knownPlayers[UUID] = { name, lastHit: hit() }
  db.put(UUID, name, err => {
    if (err) {
      return cb(err)
    }
    console.log(`putPlayer: ${name}`)
    cb(null, name)
  })
}

const getPlayer = (UUID, cb) => {
  if (UUID in knownPlayers) {
    knownPlayers[UUID].lastHit = hit()
    return cb(null, knownPlayers[UUID].name)
  }
  db.get(UUID, (err, name) => {
    if (err || !name) {
      return cb(new Error('cannot find name'))
    }
    console.log(`getPlayer: ${name}`)
    knownPlayers[UUID] = { name, lastHit: hit() }
    cb(null, name)
  })
}

const isHisTurn = UUID =>
  state.word.length % state.players.length === state.players.indexOf(UUID)

const say = (UUID, leftside, char, cb) => {
  if (!isHisTurn(UUID)) {
    return cb(new Error('not your turn'))
  }
  state.word = leftside ? char + state.word : state.word + char
  cb()
}

const activatePlayer = (UUID, cb) => {
  if (!state.players.includes(UUID)) {
    state.players = [...state.players, UUID]
  }
  cb()
}

const getState = () => ({
  ...state,
  players: state.players.map(player => knownPlayers[player].name)
})

const restart = (cb) => {
  state.word = ''
  cb()
}

module.exports = {
  getPlayer,
  putPlayer,
  activatePlayer,
  restart,
  say,
  getState
}
