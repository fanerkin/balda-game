const { putPlayer } = require('../store')

const giveName = (req, res, next) => {
  const { body } = req

  putPlayer(req.cookies.UUID, body.name, err => {
    if (err) {
      next(err)
    } else {
      next()
    }
  })
}

module.exports = giveName
