const uuid4 = require('uuid/v4')

const handleCookie = (req, res, next) => {
  let { UUID } = req.cookies

  if (!UUID) {
    UUID = uuid4()
    req.cookies.UUID = UUID
  }

  res.cookie('UUID', UUID, { maxAge: 31536000000 })
  next()
}

module.exports = handleCookie
