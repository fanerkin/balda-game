const store = require('../store')

const output = (req, res, next) => res.json({
  success: true,
  state: store.getState()
})

module.exports = output
