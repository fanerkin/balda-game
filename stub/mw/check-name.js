const { getPlayer } = require('../store')

const checkName = (req, res, next) => {
  const { UUID } = req.cookies

  getPlayer(UUID, (err, name) => {
    if (err) {
      next(err)
    } else {
      next()
    }
  })
}

module.exports = checkName
