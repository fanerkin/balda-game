const store = require('../store')

const activatePlayer = (req, res, next) => {
  const { UUID } = req.cookies

  store.activatePlayer(UUID, err => {
    if (err) {
      next(err)
    } else {
      next()
    }
  })
}

module.exports = activatePlayer
