const store = require('../store')

const say = (req, res, next) => {
  const { body } = req

  if (body.act !== 'say') {
    return next()
  }

  store.say(req.cookies.UUID, body.leftside, body.char, err => {
    if (err) {
      next(err)
    } else {
      next()
    }
  })
}

module.exports = say
