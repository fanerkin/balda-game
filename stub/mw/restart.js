const { restart } = require('../store')

const restartMw = (req, res, next) => {
  const { body } = req

  if (body.act !== 'restart') {
    return next()
  }

  restart(err => {
    if (err) {
      next(err)
    } else {
      next()
    }
  })
}

module.exports = restartMw
