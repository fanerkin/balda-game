const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const output = require('./mw/output')
const say = require('./mw/say')
const checkName = require('./mw/check-name')
const giveName = require('./mw/give-name')
const addPlayer = require('./mw/add-player')
const handleCookie = require('./mw/handle-cookie')
const restart = require('./mw/restart')
const app = express()
const jsonParser = bodyParser.json()

app.use(cookieParser())
app.use(handleCookie)
app.put('/balda/api', jsonParser)
app.put('/balda/api', giveName)
app.use(checkName)
app.use(addPlayer)

app.post('/balda/api', jsonParser)
app.post('/balda/api', say)
app.post('/balda/api', restart)

app.use(output)
app.use((err, req, res, next) => {
  if (!res.status) {
    res.status(500)
  }
  res.json({ success: false, error: err.message })
})

app.listen(3000, () => {
  console.log('Example app listening on port 3000!')
})
